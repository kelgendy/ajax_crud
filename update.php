<?php
// include to get database connection
include_once 'config/database.php';

try{
	// PDO update query
	$query = "UPDATE  
	users 
	set 
	name = :name, 
	email = :email, 
	image = :image, 
	lat = :lat, 
	lng = :lng 
	where
	id = :id";

	// prepare query for execution
	$stmt = $con->prepare($query);

	if(!empty($_FILES['file']['name'])) {
		// upload user image
		// storing source path of the file in a variable
		$sourcePath = $_FILES['file']['tmp_name'];
		$newImage = htmlspecialchars(strip_tags($_FILES['file']['name']));
		$targetDir = "uploads/" . htmlspecialchars(strip_tags($_POST['id']));
		if (!file_exists($targetDir)) {
			mkdir($targetDir, 0777, true);
		}
		// target path where file is to be stored
		$targetPath = $targetDir . "/" . $newImage;

		// moving Uploaded file
		move_uploaded_file($sourcePath,$targetPath) ;

		echo "User image uploaded.";
	} else {
		$newImage = htmlspecialchars(strip_tags($_POST['oldImage']));
	}

	// posted values
	$name=htmlspecialchars(strip_tags($_POST['name']));
	$email=htmlspecialchars(strip_tags($_POST['email']));
	$image=$newImage;
	$lat=htmlspecialchars(strip_tags($_POST['lat']));
	$lng=htmlspecialchars(strip_tags($_POST['lng']));
	$id=htmlspecialchars(strip_tags($_POST['id']));

	// bind the parameters
	$stmt->bindParam(':name', $name);
	$stmt->bindParam(':email', $email);
	$stmt->bindParam(':image', $image);
	$stmt->bindParam(':lat', $lat);
	$stmt->bindParam(':lng', $lng);
	$stmt->bindParam(':id', $id);

	// execute the query
	if($stmt->execute()){
		echo "User was updated.";
	}else{
		echo "Unable to update user.";
	}
}

// handle error
catch(PDOException $exception){
	echo "Error: " . $exception->getMessage();
}
?>