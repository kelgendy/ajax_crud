<?php
// include to get database connection
include_once 'config/database.php';

try{

	// set your default time-zone
	date_default_timezone_set('UTC');
	$created=date('Y-m-d H:i:s');

	// write query
	$query = "INSERT INTO users SET name=:name, email=:email, image=:image, lat=:lat, lng=:lng, created=:created";

	// prepare query for execution
	$stmt = $con->prepare($query);

	// posted values
	$name=htmlspecialchars(strip_tags($_POST['name']));
	$email=htmlspecialchars(strip_tags($_POST['email']));
	$image=htmlspecialchars(strip_tags($_FILES['file']['name']));
	$lat=htmlspecialchars(strip_tags($_POST['lat']));
	$lng=htmlspecialchars(strip_tags($_POST['lng']));

	// bind the parameters
	$stmt->bindParam(':name', $name);
	$stmt->bindParam(':email', $email);
	$stmt->bindParam(':image', $image);
	$stmt->bindParam(':lat', $lat);
	$stmt->bindParam(':lng', $lng);
	$stmt->bindParam(':created', $created);

	// execute the query
	if($stmt->execute()){
		echo "User was created.";
		$userID = $con->lastInsertId();

		// upload user image
		// storing source path of the file in a variable
		$sourcePath = $_FILES['file']['tmp_name']; 
		$targetDir = "uploads/" . $con->lastInsertId();
		if (!file_exists($targetDir)) {
			mkdir($targetDir, 0777, true);
		}
		
		// target path where file is to be stored
		$targetPath = $targetDir . "/" . $image;

		// moving Uploaded file
		move_uploaded_file($sourcePath,$targetPath) ; 

		echo "User image uploaded.";
	}else{
		echo "Unable to create user.";
	}
}

// handle error
catch(PDOException $exception){
	echo "Error: " . $exception->getMessage();
}

?>