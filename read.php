<?php
// include to get database connection
include 'config/database.php';

// PDO select all query
$query = "SELECT id, name, email, image, lat, lng, created FROM users ORDER BY id desc";
$stmt = $con->prepare($query);
$stmt->execute();

// get number of rows returned
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){

	// start table
	echo "<table class='table table-bordered table-hover'>";

	// creating our table heading
	echo "<tr>";
	echo "<th>Name</th>";
	echo "<th class='width-30-pct'>Email</th>";
	echo "<th>Image</th>";
	echo "<th>Location</th>";
	echo "<th>Created</th>";
	echo "<th class='width-20-pct' style='text-align:center;'>Action</th>";
	echo "</tr>";

	// retrieve our table contents
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		// extract row
		extract($row);

		// creating new table row per record
		echo "<tr>";
		echo "<td>{$name}</td>";
		echo "<td>{$email}</td>";
		echo "<td><img class='circle-img-small' src='uploads/{$id}/{$image}' /></td>";

		// google map
		echo "<td>";
		echo "<div class='small-map'></div>";
		echo "<div class='display-none lat'>{$lat}</div>";
		echo "<div class='display-none lng'>{$lng}</div>";
		echo "</td>";
		echo "<td>{$created}</td>";

		echo "<td style='text-align:center;'>";
		// add the record id
		echo "<div class='user-id display-none'>{$id}</div>";

		// edit button
		echo "<div class='btn btn-info edit-btn margin-right-1em'>";
		echo "<span class='glyphicon glyphicon-edit'></span> Edit";
		echo "</div>";

		// delete button
		echo "<div class='btn btn-danger delete-btn'>";
		echo "<span class='glyphicon glyphicon-remove'></span> Delete";
		echo "</div>";
		echo "</td>";
		echo "</tr>";
	}

	//end table
	echo "</table>";

}

// no records found
else{
	echo "<div class='noneFound'>No records found.</div>";
}

?>