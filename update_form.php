<?php
//include to get database connection
include 'config/database.php';

// get user id
$user_id=isset($_GET['user_id']) ? $_GET['user_id'] : die('ERROR: User ID not found.');

// PDO query to select single record based on ID
$query = "SELECT 
id, name, email, image, lat, lng 
FROM 
users  
WHERE 
id = ? 
LIMIT 0,1";

// prepare query
$stmt = $con->prepare($query);

// this is the first question mark on the query
$stmt->bindParam(1, $user_id);

// execute our query
if($stmt->execute()){

	// store retrieved row to a variable
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	// values to fill up our form
	$id = $row['id'];
	$name = $row['name'];
	$email = $row['email'];
	$image = $row['image'];
	$lat = $row['lat'];
	$lng = $row['lng'];

}else{
	echo "Unable to read record.";
}
?>

<form id='update-user-form' action='#' method='post' border='0' enctype="multipart/form-data">
	<table class='table table-bordered table-hover'>
		<tr>
			<td>Name</td>
			<td><input type='text' name='name' class='form-control' value='<?php echo htmlspecialchars($name, ENT_QUOTES); ?>' required /></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type='email' name='email' class='form-control' value='<?php echo htmlspecialchars($email, ENT_QUOTES); ?>' required /></td>
		</tr>
		<tr>
			<td>Image</td>
			<td>
				<img id="previewing" class="circle-img" src="uploads/<?php echo $id; ?>/<?php echo htmlspecialchars($image, ENT_QUOTES); ?>" />
				<input id="file" type='file' name='file' class='form-control' accept="image/*" />
				<input type='hidden' name='oldImage' value='<?php echo htmlspecialchars($image, ENT_QUOTES); ?>'>
			</td>
		</tr>
		<tr>
			<td>Location</td>
			<td>
				<div id="map"></div>
				<input type="hidden" name="lat" id="lat" value="<?php echo $lat ?>">
				<input type="hidden" name="lng" id="lng" value="<?php echo $lng ?>">
			</td>
		</tr>
		<tr>
			<td>
				<!-- hidden ID field so that we could identify what record is to be updated -->
				<input type='hidden' name='id' value='<?php echo $id ?>' /> 
			</td>
			<td>
				<button type='submit' class='btn btn-primary'>
					<span class='glyphicon glyphicon-edit'></span> Save Changes
				</button>
			</td>
		</tr>
	</table>
</form>