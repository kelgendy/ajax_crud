<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Users</title>

	<!-- bootstrap CSS -->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- custom CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">

</head>
<body>

	<!-- container -->
	<div class="container">

		<div class='page-header'>
			<h1 id='page-title'>Users</h1>
		</div>

		<div class='margin-bottom-1em overflow-hidden'>
			<!-- when clicked, it will show the users list -->
			<div id='users-list' class='btn btn-primary pull-right display-none'>
				<span class='glyphicon glyphicon-list'></span> Users List
			</div>

			<!-- when clicked, it will load the new user form -->
			<div id='new-user' class='btn btn-primary pull-right'>
				<span class='glyphicon glyphicon-plus'></span> New User
			</div>

			<!-- this is the loader image, hidden at first -->
			<div id='loader-image'><img src='assets/images/loader.gif' /></div>
		</div>

		<div id='page-content'></div>

	</div>
	<!-- /container -->

	<!-- jQuery library -->
	<script src="http://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>

	<!-- bootstrap JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- custom JS -->
	<script type="text/javascript" src="assets/js/map.js"></script>
	<script type="text/javascript" src="assets/js/main.js"></script>

	<!-- Google Maps API -->
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDePLQGDeKQGFRr9hljZEmfF-3Z2SEui10"></script>

</body>
</html>