function changePageTitle(page_title){   
	$('#page-title').text(page_title);
	document.title=page_title;
}

$(document).ready(function(){

	// view users on load of the page
	$('#loader-image').show();
	showUsers();
	
	// clicking the 'users list' button
	$('#users-list').click(function(){

		// show a loader img
		$('#loader-image').show();
		
		// show new user button
		$('#new-user').show();
		
		// hide users list button
		$('#users-list').hide();
		
		// show users
		showUsers();
	});
	
	// users list
	function showUsers(){

		// change page title
		changePageTitle('Users');
		
		// fade out effect first
		$('#page-content').fadeOut('slow', function(){
			$('#page-content').load('read.php', function(){
				// hide loader image
				$('#loader-image').hide(); 
				
				// fade in effect
				$('#page-content').fadeIn('slow');

				// initialize maps
				initSmallMaps();
			});
		});
	}

	// will show the new user form
	$('#new-user').click(function(){
			// change page title
			changePageTitle('New User');
			
			// show new user form
			// show a loader image
			$('#loader-image').show();
			
			// hide new user button
			$('#new-user').hide();
			
			// show users list button
			$('#users-list').show();
			
			// fade out effect first
			$('#page-content').fadeOut('slow', function(){
				$('#page-content').load('create_form.php', function(){ 
					// hide loader image
					$('#loader-image').hide(); 
					
					// fade in effect
					$('#page-content').fadeIn('slow');

					// initialize maps
					initMap();
				});
			});
		});

	// clicking the edit button
	$(document).on('click', '.edit-btn', function(){ 

		// change page title
		changePageTitle('Update User');
		
		var user_id = $(this).closest('td').find('.user-id').text();

		// show a loader image
		$('#loader-image').show();
		
		// hide create user button
		$('#new-user').hide();
		
		// show read users button
		$('#users-list').show();
		
		// fade out effect first
		$('#page-content').fadeOut('slow', function(){
			$('#page-content').load('update_form.php?user_id=' + user_id, function(){
				// hide loader image
				$('#loader-image').hide(); 
				
				// fade in effect
				$('#page-content').fadeIn('slow');

				// initialize maps
				initMap();
			});
		});
	});

	$(document).on('change', '#file', function() {
		var file = this.files[0];
		var imagefile = file.type;
		var match= ["image/jpeg","image/png","image/jpg"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
		{
			$('#previewing').attr('src','assets/images/default.png');
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.onload = imageLoaded;
			reader.readAsDataURL(this.files[0]);
		}
	});

	function imageLoaded(e) {
		$('#previewing').attr('src', e.target.result);
	};

	// will run if create user form was submitted
	$(document).on('submit', '#new-user-form', function() {
		// show a loader img
		$('#loader-image').show();

		// post the data from the form
		$.ajax({
			url: "create.php",
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data)
			{
				// show create user button
				$('#new-user').show();

				// hide read users button
				$('#users-list').hide();

				showUsers();
			}
		});

		return false;
	});

	// will run if update user form was submitted
	$(document).on('submit', '#update-user-form', function() {

		// show a loader img
		$('#loader-image').show();
		
		// post the data from the form
		$.ajax({
			url: "update.php",
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data)
			{
				// show create user button
				$('#new-user').show();

				// hide read users button
				$('#users-list').hide();

				showUsers();
			}
		});

		return false;
	});

	// will run if the delete button was clicked
	$(document).on('click', '.delete-btn', function(){ 
		if(confirm('Are you sure?')){

			// get the id
			var user_id = $(this).closest('td').find('.user-id').text();
			
			// trigger the delete file
			$.post("delete.php", { id: user_id })
			.done(function(data){

				// show loader image
				$('#loader-image').show();
				
				// reload the user list
				showUsers();
				
			});
		}
	});
	
});
