var marker;
function initMap() {
	// get lat and lng
	lat = $('#lat').val();
	lng = $('#lng').val();
	myLatlng = new google.maps.LatLng(lat,lng);
	myOptions = {
		zoom: 4,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	// create map
	map = new google.maps.Map(document.getElementById("map"), myOptions);

	// set marker
	marker = new google.maps.Marker({
		position: myLatlng,
		map: map
	});

	// add listener to move marker
	google.maps.event.addListener(map, 'click', function(event) {
		moveMarker(event.latLng);
	});
}

function moveMarker(location) {
	// update form fields with new lat/lng
	$('#lat').val(location.lat());
	$('#lng').val(location.lng());

	// move the marker
	marker.setPosition(location);
}

function initSmallMaps() {

	$('.small-map').each(function(i, obj) {
		// get lat and lng
		lat = $(this).closest('td').find('.lat').text();
		lng = $(this).closest('td').find('.lng').text();

		myLatlng = new google.maps.LatLng(lat, lng);
		myOptions = {
			zoom: 4,
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		// create map
		map = new google.maps.Map(obj, myOptions);

		// set marker
		marker = new google.maps.Marker({
			position: myLatlng,
			map: map
		});
	});
}