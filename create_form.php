<form id='new-user-form' action='#' method='post' border='0' enctype="multipart/form-data">
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Name</td>
			<td><input type='text' name='name' class='form-control' required /></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type='email' name='email' class="form-control" required /></td>
		</tr>
		<tr>
			<td>Image</td>
			<td>
				<img id="previewing" class="circle-img" src="assets/images/default.png" />
				<input id="file" type='file' name='file' class='form-control' required accept="image/*" />
			</td>
		</tr>
		<tr>
			<td>Location</td>
			<td>
				<div id="map"></div>
				<input type="hidden" name="lat" id="lat" value="28.149503">
				<input type="hidden" name="lng" id="lng" value="30.585937">
			</td>
		</tr>
		<tr>
			<td></td>
			<td>                
				<button type='submit' class='btn btn-primary'>
					<span class='glyphicon glyphicon-plus'></span> Create User
				</button>
			</td>
		</tr>
	</table>
</form>